# designPatternsDemo
Projects about design patterns.
23个常用模式可分为创建型模式、结构型模式和行为型模式三类。

创建型模式包括：（5种）
这些设计模式提供了一种在创建对象的同时隐藏创建逻辑的方式，而不是使用 new 运算符直接实例化对象。这使得程序在判断针对某个给定实例需要创建哪些对象时更加灵活。
工厂方法：Factory Method
抽象工厂：Abstract Factory
建造者：Builder
原型：Prototype
单例：Singleton

结构型模式有：（7种）
这些设计模式关注类和对象的组合。继承的概念被用来组合接口和定义组合对象获得新功能的方式。
适配器
桥接
组合
装饰器
外观
享元
代理

行为型模式有：（11种）
这些设计模式特别关注对象之间的通信。
责任链
命令
解释器
迭代器
中介
备忘录
观察者
状态
策略
模板方法
访问者
